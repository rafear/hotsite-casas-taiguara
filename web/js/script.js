/**
 * Created by rafael.lemos on 26/05/2015.
 */
$(function() {


    /**
     * https://css-tricks.com/snippets/jquery/smooth-scrolling/
     */
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.detalhesAula').click(function() {
        $.get( location.href.substring(0, location.href.lastIndexOf("/")+1)+$(this).data('materia')+'.html', function( data ) {
            console.log(data);
            $('#modalDetalhesAula').html(data);
            $('#modalDetalhesAula').modal();
            console.log( "Load was performed." );
        });
    });
});