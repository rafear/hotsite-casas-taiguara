<?php
require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

//template
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// ... definitions
$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig', array(
    ));
});


//Controllers
$app->mount('/aula', include 'aula.php');

$app->run();